# Social Learning Strategies

## Frequency Distribution of different Phenotypes

To test our model of social learning we compare it to the results with well known model:  for exemple the Neutral Model.


To compare the model with the neutral model usually studied in cultural evolution, a few adjustement has to be made:
In the traditional model agents are both social and "individual" learner. When they "learn" individually, they inovate at a rate $\mu$ other was they randomly copy another agent. In the current model, if social learner learn only through social learning (ie $z=1$), they can't innovate $p=p''+z(P-p'')\Rightarrow p=P$ where P is the phenotype copied.  To emulate the inovation rate of the neutral model we setup a population made of a percentage of $\mu$ individual learners that generate new phenotypes at each generation (with $\mu_x=1$) while the rest of the population ($1-\mu$) are social learn ($x,y=0,z=1$). All other mutation rates are set to 0, thus we have a rate of apparition of new phenotype of $\mu$.

To stay closer to original neutral model we introduce a simple reproduction scenario independant to fitness where every individual create one offspring (thus we have a static population size). Environment  doesn't matter thus environment will stay constant thoughout the simulations.

```{r reproNeutralDistrib,out.width="50%",fig.show="hold",fig.cap="distribution of phenotype (Complementary Cumulative Density Funcion) of a simpler version of our model without growth vs neutral model",cache=TRUE}
for(n in c(1000,10000)){
    for(mu in c(0.01,0.001)){
        m=100
        pop=generatePop(n,distrib=list(x=c(runif(n,-1,1)),y=rep(0,n),z=c(rep(1,n*(1-mu)),rep(0,n*mu))),df=F)
        pop[,"p"]=pop[,"x"]

        #this should be done with cache=TRUE instead of putting as images but anyway...
        plot(1,1,log="xy",xlab="phenotype frequencies",ylab="ccfd (%)",xlim=c(1,10000),ylim=c(0.01,100),type="n",main=bquote(mu == .(mu) ~ N == .(n)))
        for(i in 1:10){
            soci=evosolearn(pop = pop, tstep = 50,log=F,theta=rep(0,1000),sigma=c(s=10000,y=10000,z=10000),omega=0,delta=0,b=2,K=1000,m=c(x=.1,y=0,z=0),mu=c(x=mu,y=0,z=0),allpops=T,repro="unique",selection=F,statvar=c("p","x","z"),statfun="mean",E=c(x=0,y=0,z=0),sls="random") 
            u=unlist(sapply(soci$allpop,function(s)s[,"p"]))
            freqt=table(u)
            cc=ccfd(freqt)
            lines(cc[,"x"],cc[,"y" ],col="green",lwd=2)
            random=randomCascades(Nmax=n,Nmin=n,mu=mu,t_steps = 50)
            cc=ccfd(random$size)
            lines(cc[,"x"],cc[,"y" ],col="red",lwd=2)
        }
        legend("bottomleft",lwd=2,col=c("red","green"),legend=c("model from carrignon et al 2019","model pleisto evol"))
    }
}
```

When population growth is introduced the general shape changes slightly

```{r reproNeutralDistribGrowth,out.width="50%",fig.show="hold",fig.cap="distribution of phenotype (Complementary Cumulative Density Funcion) of our model with growth vs neutral model",cache=TRUE}
for(n in c(1000,10000)){
    for(mu in c(0.01,0.001)){
        m=100
        pop=generatePop(n,distrib=list(x=c(runif(n,-1,1)),y=rep(0,n),z=c(rep(1,n*(1-mu)),rep(0,n*mu))),df=F)
        pop[,"p"]=pop[,"x"]

        plot(1,1,log="xy",xlab="phenotype frequencies",ylab="ccfd (%)",xlim=c(1,10000),ylim=c(0.01,100),type="n",main=bquote(mu == .(mu) ~ N == .(n)))
        for(i in 1:10){
            soci=evosolearn(pop = pop, tstep = 50,log=F,theta=rep(0,1000),sigma=c(s=10000,y=10000,z=10000),omega=0,delta=0,b=2,K=n,m=c(x=.1,y=0,z=0),mu=c(x=mu,y=0,z=0),allpops=T,repro="asex",selection=T,statvar=c("p","x","z"),statfun="mean",E=c(x=0,y=0,z=0),sls="random") 
            u=unlist(sapply(soci$allpop,function(s)s[,"p"]))
            freqt=table(u)
            cc=ccfd(freqt)
            lines(cc[,"x"],cc[,"y" ],col="green",lwd=2)
            random=randomCascades(Nmax=n,Nmin=n,mu=mu,t_steps = 50)
            cc=ccfd(random$size)
            lines(cc[,"x"],cc[,"y" ],col="red",lwd=2)
        }
        legend("bottomleft",lwd=2,col=c("red","green"),legend=c("model from carrignon et al 2019","model pleisto evol+growth"))
    }
}
```



When individual can socially copy the best phenotype without error,  as one could expect, this best phenotype is quickly selected after few generations and get reproduced all the time. Thus a few phenotypes are repeated once or twice (the one at the initilisation and those that randomly appears through mutation) while another one (the best) is repeated all the time throughtout the simulations.

```{r reproNeutralDistribhBEST,out.width="50%",fig.show="hold",fig.cap="distribution of phenotype (Complementary Cumulative Density Funcion) of our model with BEST SLS vs neutral model",cache=TRUE}
for(n in c(1000,10000)){
    for(mu in c(0.01,0.001)){
        m=100
        pop=generatePop(n,distrib=list(x=c(runif(n,-1,1)),y=rep(0,n),z=c(rep(1,n*(1-mu)),rep(0,n*mu))),df=F)
        pop[,"p"]=pop[,"x"]

        plot(1,1,log="xy",xlab="phenotype frequencies",ylab="ccfd (%)",xlim=c(1,100000),ylim=c(0.1,100),type="n",main=bquote(mu == .(mu) ~ N == .(n)))
        for(i in 1:10){
            soci=evosolearn(pop = pop, tstep = 50,log=F,theta=rep(0,1000),sigma=c(s=10000,y=10000,z=10000),omega=0,delta=0,b=2,K=1000,m=c(x=.1,y=0,z=0),mu=c(x=mu,y=0,z=0),allpops=T,repro="asex",selection=T,statvar=c("p","x","z"),statfun="mean",E=c(x=0,y=0,z=0),sls="best") 
            u=unlist(sapply(soci$allpop,function(s)s[,"p"]))
            freqt=table(u)
            cc=ccfd(freqt)
            lines(cc[,"x"],cc[,"y" ],col="green",lwd=2)
            random=randomCascades(Nmax=n,Nmin=n,mu=mu,t_steps = 50)
            cc=ccfd(random$size)
            lines(cc[,"x"],cc[,"y" ],col="red",lwd=2)
        }
        legend("bottomleft",lwd=2,col=c("red","green"),legend=c("model from carrignon et al 2019","model pleisto evol+bestSLS"))
    }
}
```
What happen when with have a fitness proportional social learning strategie?

```{r reproNeutralDistribFitProp,out.width="50%",fig.show="hold",fig.cap="distribution of phenotype (Complementary Cumulative Density Funcion) of our model with FITPROP SLS vs neutral model,different line type represent different 'fitness transparency'",cache=TRUE}
for(n in c(1000,10000)){
    for(mu in c(0.01,0.001)){
        m=100
        pop=generatePop(n,distrib=list(x=c(runif(n,-1,1)),y=rep(0,n),z=c(rep(1,n*(1-mu)),rep(0,n*mu))),df=F)
        pop[,"p"]=pop[,"x"]

        plot(1,1,log="xy",xlab="phenotype frequencies",ylab="ccfd (%)",xlim=c(1,1000000),ylim=c(0.001,100),type="n",main=bquote(mu == .(mu) ~ N == .(n)))
        li=1
        for(lbd in c(1,10,100)){
            for(i in 1:10){
                soci=evosolearn(pop = pop, tstep = 50,log=F,theta=rep(0,1000),sigma=c(s=10000,y=10000,z=10000),omega=0,delta=0,b=2,K=1000,m=c(x=.1,y=0,z=0),mu=c(x=mu,y=0,z=0),allpops=T,repro="asex",selection=T,statvar=c("p","x","z"),statfun="mean",E=c(x=0,y=0,z=0),sls="fitprop",lbd=lbd) 
                u=unlist(sapply(soci$allpop,function(s)s[,"p"]))
                freqt=table(u)
                cc=ccfd(freqt)
                lines(cc[,"x"],cc[,"y" ],col="green",lwd=2,lty=li)
                li=li+1
            }
        }
        for(i in 1:10){
            random=randomCascades(Nmax=n,Nmin=n,mu=mu,t_steps = 50)
            cc=ccfd(random$size)
            lines(cc[,"x"],cc[,"y" ],col="red",lwd=2)
        }
        legend("bottomleft",lwd=2,col=c("red","green"),legend=c("model from carrignon et al 2019","model pleisto evol+fitpropSLS"))
    }
}
```



## Reproducing Rogers paradox

We detected the Rogers paradox in our simulation when counting the proportion of different strategies n our population, we show that, by looking at large set of simulation with different mutation rate, $k_z$ and $k_y$,  when population tends to be made of only social learner the fitness of this population drops ([here](http://www.dysoc.org/~simon/report/exploring-impact-of-cost-and-selective-pressure-sexual-reproduction.html) and [here](http://www.dysoc.org/~simon/report/exploring-impact-of-cost-and-selective-pressure-asexual-reproduction.html)).

Here we reproduce this effect using a different setup closer to the original Rogers publication with no genetic adaptation. To do so we need a environment that changes at a rate $u=.8$ in a discrete way. Then we need parameters set in a way that if behavior doesnt match the environment the fitness of individual is 0 where whn it matches the fitness of individual learner is less than the fitness of social learned. To achieve that we choose:

* $\sigma_s =0.2$
* $\sigma_y =0.8$
* $\sigma_z =4$
* Genotype of Individual Leaners $i$:  $y_i=1,z_i=0$
* Genotype of Social Leaners $s$:  $y_s=0,z_s=1$

Thus :

* fitness of individual learner $i$ when match is: $w_i\approx$ `r round(fitness(1,1,0,1,0,.2,.8,4),digits=2)`
* fitness of social learner $s$ when match is: $w_s\approx$ `r round(fitness(1,1,0,0,1,.2,.8,4),digits=2)`
* fitness of $i$ when mismmatch is: $w_i\approx$ `r round(fitness(0,1,0,1,0,.2,.8,4),digits=2)`
* fitness of $s$ when mismatch is: $w_s\approx$ `r round(fitness(0,1,0,0,1,.2,.8,4),digits=2)`

First we run the simulation without evolution (no mutation, no reproduction) to check the expected fitness of the different type of individual  in our model given the parameters chosen and see if this reproduce Rogers Fig 1. 

```{r rogers1988,out.width="50%",fig.align="center",fig.cap="Comparing Rogers (1988), Fig 1. (left) with our model (right)",fig.show="hold",cache=TRUE}
tsteps=1000
env=rep(1,tsteps)
for(i in 2:length(env)){
    if(runif(1)<.8){
        if(runif(1)<.5)
            env[i]=env[i-1]+1
        else
            env[i]=env[i-1]
    }
    else env[i]=env[i-1]
}

freqSL=seq(.001,.999,length.out=10)
allfitness=lapply(freqSL,function(percentage){
                  n=1000
                  pop=generatePop(n,distrib=list(x=rep(0,n),y=rep(0,n),z=rep(0,n)))
                  pop[,"p"]=!env[1]
                  sl=1:(n*percentage)
                  il=(n*percentage+1):n
                  pop[sl,"z"]=1
                  pop[il,"y"]=1

                  soci=evosolearn(pop = pop, tstep = tsteps,log=F,theta=env,sigma=c(s=.2,y=.8,z=4),omega=0,delta=0,b=2,K=1000,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),allpops=T,repro="unique",selection=F,statvar=c("p","x","z","w"),statfun="mean",E=c(x=0,y=0,z=0),sls="random")
                  soci

})
meanAll=sapply(allfitness,function(soci){
                  cbind(y=mean(sapply(soci$allpop,function(i){mean(i[i[,"y"]==1,"w"])}),na.rm=T),
                        z=mean(sapply(soci$allpop,function(i){mean(i[i[,"z"]==1,"w"])}),na.rm=T),
                        pop=mean(sapply(soci$allpop,function(i){mean(i[,"w"])}),na.rm=T))
})

plot(1,1,type="n",ylim=c(0,1),xlim=c(0,1),xlab="% social learner",ylab="fitness")
for(i in 1:3){
   lines(freqSL,meanAll[i,],col=i,lwd=3)
}
legend("bottom",legend=c("IL","SL","pop"),col=1:3,lwd=3)
```
 
If social learners can copy the best phenotype this disappear, unless there is no individual learner at all, all social learner can find and copy the good phenotype. 

```{r rogerFitprop,out.width="50%",fig.align="center",fig.cap="When social learners are able to know about the fitness of the individual and select a phenotypes given this information, the Rogers effect disappear",cache=TRUE,fig.show="hold"}

for( sls in c("random","best","fitprop","distprop")){
    tsteps=1000
    env=rep(1,tsteps)
    for(i in 2:length(env)){
        if(runif(1)<.8){
            if(runif(1)<.5)
                env[i]=env[i-1]+1
            else
                env[i]=env[i-1]
        }
        else env[i]=env[i-1]
    }

    freqSL=seq(.001,.999,length.out=10)
    allfitness=lapply(freqSL,function(percentage){
                      n=1000
                      pop=generatePop(n,distrib=list(x=rep(0,n),y=rep(0,n),z=rep(0,n)))
                      pop[,"p"]=env[1]
                      sl=1:(n*percentage)
                      il=(n*percentage+1):n
                      pop[sl,"z"]=1
                      pop[il,"y"]=1

                      soci=evosolearn(pop = pop, tstep = tsteps,log=F,theta=env,sigma=c(s=.2,y=.8,z=4),omega=0,delta=0,b=2,K=1000,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),allpops=T,repro="unique",selection=F,statvar=c("p","x","z","w"),statfun="mean",E=c(x=0,y=0,z=0),sls=sls)
                      soci

    })
    meanAll=sapply(allfitness,function(soci){
                      cbind(y=mean(sapply(soci$allpop,function(i){mean(i[i[,"y"]==1,"w"])}),na.rm=T),
                            z=mean(sapply(soci$allpop,function(i){mean(i[i[,"z"]==1,"w"])}),na.rm=T),
                            pop=mean(sapply(soci$allpop,function(i){mean(i[,"w"])}),na.rm=T))
    })

    plot(1,1,type="n",ylim=c(0,1),xlim=c(0,1),xlab="% social learner",ylab="fitness",main=sls)
    for(i in 1:3){
       lines(freqSL,meanAll[i,],col=i,lwd=3)
    }
    legend("bottom",legend=c("IL","SL","pop"),col=1:3,lwd=3)
}
```

 
At an evolutionary level, the expected outcome of Rogers paradox is that Social learning should not evolve when environment is changing, as in the long term the mean fitness of a population made of social learners will decrease. Though our mutation process isn't exactly the same than Rogers (in his experiments phenotypes flip from social learners to individual learner randomly), this is what we observe with mutation and asexual reproduction (cf below). When the rate of change is high enough ($u=.8$ in Rogers model), social learning is not selected no matter the proportion of social learners in the initial population, with the risks that the whole population get extinct if social learning is fixed. 

Whereas this is not the case when environmental changes is very unlikely(here proba of change $u=0.01$)

```{r rogerEvoslow,out.width="50%",fig.align="center",fig.cap="Social learning is selected when environmental changes is high (left, $u=0.8$) but not when environmental change is slow (right, u=$0.01$)",cache=TRUE,fig.show="hold"}
tsteps=40
sls="random"

for(u in c(0.8,0.01)){
    env=rep(1,tsteps)
    for(i in 2:length(env)){
        if(runif(1)<u){
            if(runif(1)<.5)
                env[i]=!env[i-1]
            else
                env[i]=env[i-1]
        }
        else env[i]=env[i-1]
    }

    allfitness=lapply(freqSL,function(percentage){
                      n=1000
                      pop=generatePop(n,distrib=list(x=rep(0,n),y=rep(0,n),z=rep(0,n)))
                      pop[,"p"]=env[1]
                      sl=1:(n*percentage)
                      il=(n*percentage+1):n
                      pop[sl,"z"]=1
                      pop[il,"y"]=1

                      soci=evosolearn(pop = pop, tstep = tsteps,log=F,theta=env,sigma=c(s=.2,y=.8,z=4),omega=0,delta=0,b=2,K=1000,m=c(x=0,y=1,z=1),mu=c(x=0,y=.01,z=.01),allpops=T,repro="asex",selection=T,statvar=c("p","y","z","w"),statfun="mean",E=c(x=0,y=0,z=0),sls=sls)
                      soci

    })
    plot(1,1,type="n",ylim=c(0,1),xlim=c(0,tsteps),ylab="mean value of z",xlab="time",main=paste0("sls:",sls,", proba of change of theta:",u))
    cols=heat.colors(length(freqSL))
    for(i in 1:length(freqSL)){
       lines(allfitness[[i]]$summary[,"mean_z"],col=cols[i],lwd=3)
    }
    legend("right",title="% SL at t=0",legend=round(freqSL,digit=1)[seq(1,10,length.out=5)],col=cols[seq(1,10,length.out=5)],lwd=3)
}

```


## Finding the best phenotype with different Fitness (or distance to optimum) Proportional Learning  {#fitprop}

In simulations using this Fitness Proportional Social Learning, individual copy the phenotypes of individual from the reference group with a probability that depends on the fitness of the individuals in the reference group.
Thus the probability to copy the phenotype of individual $i$ is defined as:


$$ P(i) = \frac{w_i}{\Sigma_{n=1}^{N_e}(w_n)} $$

### Test Fitness Proportional Learning

To test if the fitness proportional learnign is working we setup  a simple experiment where the optimum $\theta$ is a fixe value $\theta_0$. We generate a population that won't evolve genetically, where $x$ and $y$ are set to $0$, and $z=1$ for all the population: agents can changes their phenotypes only by copying socially. 

We initialized all phenotypes randomly with value between $0$ and $200$. $\theta$ is chosen outside this range, as $\theta_0=250$.  This means no one has a good phenotype at the begin, thoughtsome are closer than others. We add some noise on the copy of the phenotype: $E_z=5$ , to allow for the pool of phenotypes to change. 
Note that results are the same if $0<\theta_0<200$, convergence is only quicker. 

During the simulation agents are not selected given their fitness, they perfectly reproduce themselves once, as if it was the same individual styaing throughout the simulation. Phenotypes are copied given random and fitprop SLS, which while change their initial distribution. 

$\sigma_s$ is set to $100$, this is use only for the computation of the fitness and doesn't matter for the survival of the individual as there is not fitness-based selection. 

$sigma_y$ doesn't matter as $y$ is set to 0 and doesn't evolve. The value of $\sigma_z$ is set to 100 but doesn't matter much neither, as $z=1$ for everyon and doesn't change. The cost of social learning will then be the same for all individual and won't affect the output of the fitness proportional social learning.

The Figure \ref@(figpopsize-fitprop) show the results of the simulations for two different population size.

```{r popsize-fitprop,cache=TRUE,fig.cap="compare effect of fitprop social learning vs random social learning. Population of 100Agents (left) and 1000 Agents (right)",out.width="50%",fig.show="hold"}
for(n in c(100,1000)){
    pop=generatePop(n)
    pop[,"p"]=sample.int(200,100)
    pop[,"x"]=0
    pop[,"z"]=1
    allrandom=replicate(10,evosolearn(pop=pop,tstep=100,theta=rep(250,100),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls="random",allpop=F,selection=F,repro="unique")[,"mean_p"],simplify=F)
    allfitprop=replicate(10,evosolearn(pop=pop,tstep=100,theta=rep(250,100),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls="fitprop",allpop=F,selection=F,repro="unique")[,"mean_p"],simplify=F)
    alldistprop=replicate(10,evosolearn(pop=pop,tstep=100,theta=rep(250,100),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls="distprop",allpop=F,selection=F,repro="unique")[,"mean_p"],simplify=F)
    plot(1,1,xlim=c(0,100),ylim=c(-50,300),type="n",xlab="time",ylab=expression(bar(p)))
    abline(h=250,col="red")
    na=lapply(allrandom,lines)
    na=lapply(allfitprop,lines,col="green")
    na=lapply(alldistprop,lines,col="blue")
    legend("bottomleft",lwd=2,col=c("black","green","blue","red"),legend=c("random","fitprop","distprop",expression(theta)))
}
```


When we look at how close to the optimum $\theta$ the phenotypes are (figure \@ref(fig:fitprop-limited)), we can see that this distance tends to increase when social learning is random and stay stable a close to $\theta$ with fitness proportional social learning. At the same time, when we compare this distance with the proportion of the reference group the social learners copy from (right column), the distance toward the optimum decrease for fitprop but doesn'tmater whe the copy is random (to compute the distance for the panels on the right column we compute the mean distance to $\theta$ during a certain number of time step.


```{r fitprop-limited,cache=TRUE,out.width="50%",fig.show="hold",fig.cap="Compare effect of choosing among a given proportion from the reference group using randomly choose agent (top) or fitpop (bottom). Population size:  1000 Agents"}
for(sls in c("random","fitprop","distprop")){
    n=500
    pop=generatePop(n)
    pop[,"p"]=sample.int(200,n,replace=T)
    pop[,"x"]=0
    pop[,"z"]=1

    maxns=seq(.1,1,.05)
    listmaxns=list()
    theta=100
    tsteps=100
    for( mn in maxns){
        listmaxns[[as.character(mn)]]=replicate(50,abs(evosolearn(pop=pop,tstep=tsteps,theta=rep(theta,tsteps),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls=sls,allpop=F,selection=F,repro="unique",maxn=mn)[,"mean_p"]-theta),simplify=F)
    }

    mncols=colorRampPalette(c("yellow","purple","blue"))(length(maxns))
    names(mncols)=as.character(maxns)

    plot(1,1,xlim=c(0,tsteps),ylim=range(unlist(listmaxns),na.rm=T),type="n",xlab="time",ylab=expression(abs(theta-bar(p))),main=paste0("SLS=",sls))
    for( mn in maxns){
        na=lapply(listmaxns[[as.character(mn)]],lines,col=mncols[as.character(mn)])
    }

    summarized=do.call("rbind",lapply(maxns,function(mn)cbind(mn,dist=sapply(listmaxns[[as.character(mn)]],function(u)mean(u[10:tsteps])))))

    plot(summarized[,1],summarized[,2],col=mncols[as.character(summarized[,1])],pch=20,xlab="% of the pop",ylab=expression(mean(abs(theta-bar(p)))),main=paste0("SLS=",sls))
}
```



The effect of fitprop copying  on the speed of the code has no impact:

```{r profiling-fitprop,fig.cap="profilingfitprop", fig.show="hold",cache=TRUE}
library(microbenchmark)
for(sls in c("fitprop","distprop","random")){
timings=lapply(maxns,function(mn){
         microbenchmark(
            evosolearn(pop=pop,tstep=tsteps,theta=rep(theta,2000),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls=sls,allpop=F,selection=F,repro="unique",maxn=mn),
                            times=5,
                            unit="s")
}
             )
timings=sapply(timings,function(i)mean(i[,"time"])/10000000000)
plot(maxns,timings,main=sls)
}
```


### Non linear Fitness proportional and proportional to distance to optimum:
 
We introduced a lambda term to increase the probability to copy the good phenotype:

```{r lambda-fitprop,cache=TRUE,fig.cap="compare effect of lambda ",out.width="50%",fig.show="hold"}
colLBD=rev(topo.colors(6))
lbds=5^(-2:3)
opt=500
for(n in c(100,1000)){
    for(sls in c("fitprop","distprop")){
        pop=generatePop(n)
        pop[,"p"]=sample.int(200,100)
        pop[,"x"]=0
        pop[,"z"]=1
        allrandom=replicate(10,evosolearn(pop=pop,tstep=300,theta=rep(opt,100),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls="random",allpop=F,selection=F,repro="unique")[,"mean_p"],simplify=F)
        allfitprop=replicate(10,evosolearn(pop=pop,tstep=300,theta=rep(opt,100),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls=sls,allpop=F,selection=F,repro="unique")[,"mean_p"],simplify=F)
        plot(1,1,xlim=c(0,100),ylim=c(-300,600),type="n",xlab="time",ylab=expression(bar(p)),main=sls)
        na=lapply(allrandom,lines,lty=2)
        na=lapply(allfitprop,lines)
        for(i in seq_along(lbds) ){
            l=lbds[i]
            allfitprop=replicate(10,evosolearn(pop=pop,tstep=300,theta=rep(opt,100),b=2,K=100,m=c(x=0,y=0,z=0),mu=c(x=0,y=0,z=0),E=c(x=0,y=0,z=5),sigma=c(s=100,y=100,z=100),log=F,sls=sls,lin=F,lbd=l,allpop=F,selection=F,repro="unique")[,"mean_p"],simplify=F)
            na=lapply(allfitprop,lines,col=colLBD[i])
        }
        abline(h=opt,col="red",lwd=2)
        legend("bottomright",lwd=1,col=c("black",1,"red",colLBD),legend=c("random","linear",expression(theta),paste("l=",lbds)),lty=c(2,1,1,1,1,1,1,1,1))
    }
}
```
