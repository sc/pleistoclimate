# Exploring Full Genome with simulated environment

## Random Social Learning

So far all simulations with the full genome are done with a cost for y and z = 1

```{r,cache=TRUE,fig.cap="Low rate of change  ($vt = 0.01$) Random Social Learning (top), Best Social Learning (bottom)  high $\\mu=0.01$ (left), low $\\mu = 0.001$ (right)",fig.show="hold",out.width="50%"}
#dasdas

moving_thetaRandom=getAllFromFolder("fullGenomesAllmeansRandom",pref="output",extinctions=TRUE)
moving_theta=getAllFromFolder("fullGenomesAllmeans",pref="output", extinctions=TRUE)
moving_thetaRandom$distX=abs(moving_thetaRandom$theta - moving_thetaRandom$mean_x)
moving_thetaBest=moving_theta
moving_thetaBest$distX=abs(moving_thetaBest$theta - moving_thetaBest$mean_x)

uniquesetupRandomMULT=moving_thetaRandom[ moving_thetaRandom$sigma == 1 & moving_thetaRandom$delta == 1 & moving_thetaRandom$m == .5 & moving_thetaRandom$vt == .001 & moving_thetaRandom$K == 2000 & moving_thetaRandom$mu == 0.01 & moving_thetaRandom$omega == 4,]
uniquesetupRandomMuLT=moving_thetaRandom[ moving_thetaRandom$sigma == 1 & moving_thetaRandom$delta == 1 & moving_thetaRandom$m == .5 & moving_thetaRandom$vt == .001 & moving_thetaRandom$K == 2000 & moving_thetaRandom$mu == 0.001 & moving_thetaRandom$omega == 4,]
uniquesetupBestMULT=moving_thetaBest[ moving_thetaBest$sigma == 1 & moving_thetaBest$delta == 1 & moving_thetaBest$m == .5 & moving_thetaBest$vt == .001 & moving_thetaBest$K == 2000 & moving_thetaBest$mu == 0.01 & moving_thetaBest$omega == 4,]
uniquesetupBestMuLT=moving_thetaBest[ moving_thetaBest$sigma == 1 & moving_thetaBest$delta == 1 & moving_thetaBest$m == .5 & moving_thetaBest$vt == .001 & moving_thetaBest$K == 2000 & moving_thetaBest$mu == 0.001 & moving_thetaBest$omega == 4,]

uniquesetupRandomMUHT=moving_thetaRandom[ moving_thetaRandom$sigma == 1 & moving_thetaRandom$delta == 1 & moving_thetaRandom$m == .5 & moving_thetaRandom$vt == .02 & moving_thetaRandom$K == 2000 & moving_thetaRandom$mu == 0.01 & moving_thetaRandom$omega == 4,]
uniquesetupRandomMuHT=moving_thetaRandom[ moving_thetaRandom$sigma == 1 & moving_thetaRandom$delta == 1 & moving_thetaRandom$m == .5 & moving_thetaRandom$vt == .02 & moving_thetaRandom$K == 2000 & moving_thetaRandom$mu == 0.001 & moving_thetaRandom$omega == 4,]
uniquesetupBestMUHT=moving_thetaBest[ moving_thetaBest$sigma == 1 & moving_thetaBest$delta == 1 & moving_thetaBest$m == .5 & moving_thetaBest$vt == .02 & moving_thetaBest$K == 2000 & moving_thetaBest$mu == 0.01 & moving_thetaBest$omega == 4,]
uniquesetupBestMuHT=moving_thetaBest[ moving_thetaBest$sigma == 1 & moving_thetaBest$delta == 1 & moving_thetaBest$m == .5 & moving_thetaBest$vt == .02 & moving_thetaBest$K == 2000 & moving_thetaBest$mu == 0.001 & moving_thetaBest$omega == 4,]

plotDistVsFitness(uniquesetupRandomMULT,main="mu=0.01, random copy")
plotDistVsFitness(uniquesetupRandomMuLT,main="mu=0.001, random copy")
plotDistVsFitness(uniquesetupBestMULT,main="mu=0.01, copy the best")
plotDistVsFitness(uniquesetupBestMuLT,main="mu=0.001, copy the best")
```



```{r,cache=TRUE,fig.cap="High rate of change ($vt = 0.2$)Random Social Learning (top), Best Social Learning (bottom)  high $\\mu=0.01$ (left), low $\\mu = 0.001$ (right)",fig.show="hold"}
par(mfrow=c(2,2))
plotDistVsFitness(uniquesetupRandomMUHT,main="mu=0.01, random copy")
plotDistVsFitness(uniquesetupRandomMuHT,main="mu=0.001, random copy")
plotDistVsFitness(uniquesetupBestMUHT,main="mu=0.01, copy the best")
plotDistVsFitness(uniquesetupBestMuHT,main="mu=0.001,copy the best")
```



```{r,cache=TRUE,fig.cap="Low rate of change ($vt = 0.01$)Random Social Learning (top), Best Social Learning (bottom)  high $\\mu=0.01$ (left), low $\\mu = 0.001$ (right)",fig.show="hold"}
par(mfrow=c(2,2))
plot3Genes(uniquesetupRandomMULT,main="mu=0.01, random copy")
plot3Genes(uniquesetupRandomMuLT,main="mu=0.001, random copy")
plot3Genes(uniquesetupBestMULT,main="mu=0.01, copy the best")
plot3Genes(uniquesetupBestMuLT,main="mu=0.001, copy the best")
```



```{r,cache=TRUE,fig.cap="High rate of change($vt = 0.2$) Random Social Learning (top), Best Social Learning (bottom)  high $\\mu=0.01$ (left), low $\\mu = 0.001$ (right)",fig.show="hold"}
par(mfrow=c(2,2))
plot3Genes(uniquesetupRandomMUHT,main="mu=0.01, random copy")
plot3Genes(uniquesetupRandomMuHT,main="mu=0.001, random copy")
plot3Genes(uniquesetupBestMUHT,main="mu=0.01, copy the best")
plot3Genes(uniquesetupBestMuHT,main="mu=0.001, copy the best")
```

```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Compare $\\omega$, $\\delta$, and rate of increase, with $\\mu = 0.01$ and $m=.5$",fig.show="hold"}
MMTR=moving_thetaRandom[ moving_thetaRandom$sigma == 1 & moving_thetaRandom$m == .5  & moving_thetaRandom$K == 2000 & moving_thetaRandom$mu == 0.01 ,]
ds=unique(MMTR$delta)
vts=unique(MMTR$vt)
colds=rev(colorRampPalette(c("red","blue"))(length(ds)))
names(colds)=ds
par(mfrow=c(3,3))
for(d in unique(MMTR$omega)){
    for(var in c("mean_z","mean_y","distX")){
        vtDelta=tapply(MMTR[,var][MMTR$omega==d],MMTR[MMTR$omega == d,c("vt","delta")],mean)
        plot(1,1,type="n",main=bquote(omega==.(d)),ylim=range(vtDelta,na.rm=T),xlim=range(vts),xlab="vt",ylab=var)
        for(i in ds)lines(vts,vtDelta[,as.character(i)],col=colds[as.character(i)],lwd=2)
    }
}
legend("topleft",legend=sapply(ds,function(d)as.expression(bquote(delta==.(d)))),lwd=2,lty=1,col=colds)
```



```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Compare $\\omega$, $\\delta$, and rate of increase, with $\\mu = 0.001$ and $m=.2$",fig.show="hold"}
MMTR=moving_thetaRandom[ moving_thetaRandom$sigma == 1 & moving_thetaRandom$m == .2  & moving_thetaRandom$K == 2000 & moving_thetaRandom$mu == 0.001 ,]
ds=unique(MMTR$delta)
vts=unique(MMTR$vt)
colds=colorRampPalette(c("red","blue"))(length(ds))
names(colds)=ds
par(mfrow=c(3,3))
for(d in unique(MMTR$omega)){
    for(var in c("mean_z","mean_y","distX")){
        vtDelta=tapply(MMTR[,var][MMTR$omega==d],MMTR[MMTR$omega == d,c("vt","delta")],mean)
        plot(1,1,type="n",main=bquote(omega==.(d)),ylim=range(vtDelta,na.rm=T),xlim=range(vts),xlab="vt",ylab=var)
        for(i in ds)lines(vts,vtDelta[,as.character(i)],col=colds[as.character(i)],lwd=2)
    }
}
legend("topleft",legend=sapply(ds,function(d)as.expression(bquote(delta==.(d)))),lwd=2,lty=1,col=colds)
```

Check the trajectory of  the effective population size with rate of changes = 0.02 and autocorrelation $\omega = 4$

```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Population size through time with random copy, v=0.02, $\\omega =4$",fig.show="hold"}
    plotAllTrajVar(moving_thetaRandom[moving_thetaRandom$vt == 0.02& moving_thetaRandom$omega == 4,],m=0.5,E=0,obs="N")
```

```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Population size through time with copy the best SLS, v=0.02, $\\omega =4$",fig.show="hold"}
    plotAllTrajVar(moving_thetaBest[moving_thetaBest$vt == 0.02& moving_thetaBest$omega == 4,],m=0.5,E=0,obs="N")
```


```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Distance to $\\theta$ through time with random copy, v=0.02, $\\omega =4$",fig.show="hold"}
    plotAllTrajVar(moving_thetaRandom[moving_thetaRandom$vt == 0.02& moving_thetaRandom$omega == 4,],m=0.5,E=0,obs="dist")
```

```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Distance to $\\theta$ through time with copy the best SLS, v=0.02, $\\omega =4$",fig.show="hold"}
    plotAllTrajVar(moving_thetaBest[moving_thetaBest$vt == 0.02& moving_thetaBest$omega == 4,],m=0.5,E=0,obs="dist")
```


```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Fitness to $w$ through time with Random copy, v=0.02, $\\omega =4$",fig.show="hold"}
    plotAllTrajVar(moving_thetaRandom[moving_thetaRandom$vt == 0.02& moving_thetaRandom$omega == 4,],m=0.5,E=0,obs="mean_w")
```

```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Fitness to $w$ through time with copy the best SLS, v=0.02, $\\omega =4$",fig.show="hold"}
    plotAllTrajVar(moving_thetaBest[moving_thetaBest$vt == 0.02& moving_thetaBest$omega == 4,],m=0.5,E=0,obs="mean_w")
```

### Value at equilibrium

```{r,fig.width=10,fig.height=10,cache=TRUE}
mtr=moving_thetaRandom[moving_thetaRandom$omega== 4 & moving_thetaRandom$vt== 0.02,]
mtb=moving_thetaBest[moving_thetaBest$omega== 4 & moving_thetaBest$vt== 0.02,]
plotAllVariableSummaries(mtr,E=0,estimate=NULL,var="distX") #we use only simulations without noise
plotAllVariableSummaries(mtr,E=0,estimate=NULL,var="mean_y") #we use only simulations without noise
plotAllVariableSummaries(mtr,E=0,estimate=NULL,var="mean_z") #we use only simulations without noise
plotAllVariableSummaries(mtb,E=0,estimate=NULL,var="distX") #we use only simulations without noise
plotAllVariableSummaries(mtb,E=0,estimate=NULL,var="mean_y") #we use only simulations without noise
plotAllVariableSummaries(mtb,E=0,estimate=NULL,var="mean_z") #we use only simulations without noise
```


## Best Social Learning

Check the trajectory of  the effective population size:
```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Effective population size through time"}
moving_theta=getAllFromFolder("fullGenomesAllmeans",pref="output", extinctions=TRUE)
rates=sort(unique(moving_theta$vt))
for(t in seq_along(rates)){
    print(paste("rate=",rates[t]))
    plotAllTrajVar(moving_theta[moving_theta$vt == rates[t],],m=0.2,E=0,obs="N")
}
```
```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="distance to optimum $\\theta$ through time"}
for(t in seq_along(rates)){
    print(paste("rate=",rates[t]))
    plotAllTrajVar(moving_theta[moving_theta$vt == rates[t],],m=0.2,E=0,obs="dist")#,ylim=c(0,.005))
}
```

```{r,fig.width=10,fig.height=10,cache=TRUE,fig.cap="Mean fitness $w$ through time"}
for(t in seq_along(rates)){
    print(paste("rate=",rates[t]))
    plotAllTrajVar(moving_theta[moving_theta$vt == rates[t],],m=0.2,E=0,obs="mean_w")#,ylim=c(0,.005))
}
```

```{r,fig.width=10,fig.height=10,cache=TRUE}
plotAllVariableSummaries(moving_theta,E=0,estimate=NULL,var="mean_x") 
plotAllVariableSummaries(moving_theta,E=0,estimate=NULL,var="mean_y") 
plotAllVariableSummaries(moving_theta,E=0,estimate=NULL,var="mean_z") 
```

## Exploring $\sigma_s$ and $\sigma_y$


```{r,eval=TRUE,cache=TRUE}
moving_theta=getAllFromFolder("genetAndILsmall",pref="output",extinctions=TRUE)
```

### Compare final mean value of x and y given $\sigma_s$ and $\sigma_y$
```{r,fig.width=7,fig.height=7,cache=TRUE}
par(mfrow=c(2,2))
for(m in unique(moving_theta$mu)){
    for(var in c("mean_x","mean_y")){
        uniquesetup=moving_theta[ moving_theta$m == .5 &  moving_theta$K == 2000 & moving_theta$mu == m ,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k))
        colsigmas=colorRampPalette(c("red","blue"))(length(sigmas))
        names(colsigmas)=sigmas
        sigmaK = tapply( uniquesetup[,var],uniquesetup[,c("sigma","k")],mean)
        plot(1,1,type="n",main=bquote(mu==.(m)),ylim=range(sigmaK,na.rm=T),xlab="k",ylab=var,xlim=range(ks))
        for(i in sigmas)lines(ks,sigmaK[as.character(i),],col=colsigmas[as.character(i)],lwd=2,type="b")
        legend("topleft",legend=sapply(sigmas,function(d)as.expression(bquote(sigma[s]==.(d)))),lwd=2,lty=1,col=colsigmas)
    }
}
```



### Mean fitness with and distance to $\theta$


Distance to optimum and fitness
```{r,cache=TRUE}
par(mfrow=c(1,2))
for(m in unique(moving_theta$mu)){
    uniquesetup=moving_theta[ moving_theta$m == .5 &  moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$sigma == 4 & moving_theta$k == .5,]
    plotDistVsFitness(uniquesetup,main=bquote(mu==.(m)))
}
```

Mean value of genes and distance between x and optimum

```{r,cache=TRUE}
par(mfrow=c(1,2))
for(m in unique(moving_theta$mu)){
        uniquesetup=moving_theta[ moving_theta$m == .5 &  moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$sigma == 4 & moving_theta$k == .5,]
plot3Genes(uniquesetup,main=bquote(mu==.(m)))
}
```


#### $\sigma_s > \sigma_y$

Distance to optimum and fitness
```{r,cache=TRUE}
par(mfrow=c(1,2))
for(m in unique(moving_theta$mu)){
        uniquesetup=moving_theta[ moving_theta$m == .5 &  moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$sigma == 4 & moving_theta$k == 2,]
plotDistVsFitness(uniquesetup,main=bquote(mu==.(m)))
}
```

Mean value of genes and distance between x and optimum

```{r,cache=TRUE}
par(mfrow=c(1,2))
for(m in unique(moving_theta$mu)){
        uniquesetup=moving_theta[ moving_theta$m == .5 &  moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$sigma == 4 & moving_theta$k == 2,]
plot3Genes(uniquesetup,main=bquote(mu==.(m)))
}
```

## Exploring Sigmas X,Y, and Z Genes


```{r,cache=TRUE}
moving_theta=getAllFromFolder("testSigmas",pref="output",extinctions=TRUE)
##moving_theta=updateScale(moving_theta)
moving_theta$distX=abs(moving_theta$theta-moving_theta$mean_x)
##save(file="data/moving_thetaXYZ.bin",moving_theta)
```

### Compare final mean value of the distance between x and $\\theta$ and the mean value of y and z 

```{r,fig.width=7,fig.height=7,cache=TRUE,fig.cap="given $\\sigma_s$ and $\\sigma_y$, Low rate of change ($vt=0.002$), $\\omega = 2$ and $m=.2$ and $2\\times\\sigma_y = \\sigma_z$"}
par(mfrow=c(2,3))
for(m in unique(moving_theta$mu)){
    for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .002 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==2& moving_theta$delta==.5,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        colsigmas=colorRampPalette(c("red","blue"))(length(sigmas))
        names(colsigmas)=sigmas
        sigmaK = tapply( uniquesetup[,var],uniquesetup[,c("sigma","k_y")],mean,na.rm=T)
        plot(1,1,type="n",main=bquote(mu==.(m)),ylim=range(sigmaK,na.rm=T),xlab="k_y",ylab=var,xlim=range(ks))
        for(i in sigmas)lines(ks,sigmaK[as.character(i),],col=colsigmas[as.character(i)],lwd=2,type="b")
        legend("topleft",legend=sapply(sigmas,function(d)as.expression(bquote(sigma[s]==.(d)))),lwd=2,lty=1,col=colsigmas)
    }
}
```

 

```{r,fig.width=7,fig.height=7,cache=TRUE,fig.cap="$\\sigma_s$ and $\\sigma_y$, $vt=0.002$ , $\\omega = 2$ and $m=.2$ and $8\\times\\sigma_y = \\sigma_z$"}
par(mfrow=c(2,3))
for(m in unique(moving_theta$mu)){
    for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .002 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==8 & moving_theta$delta==.5,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        colsigmas=colorRampPalette(c("red","blue"))(length(sigmas))
        names(colsigmas)=sigmas
        sigmaK = tapply( uniquesetup[,var],uniquesetup[,c("sigma","k_y")],mean,na.rm=T)
        plot(1,1,type="n",main=bquote(mu==.(m)),ylim=range(sigmaK,na.rm=T),xlab="k_y",ylab=var,xlim=range(ks))
        for(i in sigmas)lines(ks,sigmaK[as.character(i),],col=colsigmas[as.character(i)],lwd=2,type="b")
        legend("topleft",legend=sapply(sigmas,function(d)as.expression(bquote(sigma[s]==.(d)))),lwd=2,lty=1,col=colsigmas)
    }
}
```


```{r,fig.width=7,fig.height=7,cache=TRUE, fig.cap="Low $\\delta=.5$given $\\sigma_s$, $\\sigma_y$,$\\sigma_z$ $vt=0.2$ , $\\omega = 2$ and $m=.2$ and $2\\times\\sigma_y = \\sigma_z$"}
par(mfrow=c(2,3))
for(m in unique(moving_theta$mu)){
    for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .2 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==2 & moving_theta$delta == .5,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        colsigmas=colorRampPalette(c("red","blue"))(length(sigmas))
        names(colsigmas)=sigmas
        sigmaK = tapply( uniquesetup[,var],uniquesetup[,c("sigma","k_y")],mean,na.rm=T)
        plot(1,1,type="n",main=bquote(mu==.(m)),ylim=range(sigmaK,na.rm=T),xlab="k_y",ylab=var,xlim=range(ks))
        for(i in sigmas)lines(ks,sigmaK[as.character(i),],col=colsigmas[as.character(i)],lwd=2,type="b")
        legend("topleft",legend=sapply(sigmas,function(d)as.expression(bquote(sigma[s]==.(d)))),lwd=2,lty=1,col=colsigmas)
    }
}
```


```{r,fig.width=7,fig.height=7,cache=TRUE,fig.cap=" Low $\\delta=.5$ with $\\sigma_s$ and $\\sigma_y$, $vt=0.2$ , $\\omega = 2$ and $m=.2$ and $8\\times\\sigma_y = \\sigma_z$"}
par(mfrow=c(2,3))
for(m in unique(moving_theta$mu)){
    for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .2 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==8 & moving_theta$delta == .5,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        colsigmas=colorRampPalette(c("red","blue"))(length(sigmas))
        names(colsigmas)=sigmas
        sigmaK = tapply( uniquesetup[,var],uniquesetup[,c("sigma","k_y")],mean,na.rm=T)
        plot(1,1,type="n",main=bquote(mu==.(m)),ylim=range(sigmaK,na.rm=T),xlab="k_y",ylab=var,xlim=range(ks))
        for(i in sigmas)lines(ks,sigmaK[as.character(i),],col=colsigmas[as.character(i)],lwd=2,type="b")
        legend("topleft",legend=sapply(sigmas,function(d)as.expression(bquote(sigma[s]==.(d)))),lwd=2,lty=1,col=colsigmas)
    }
}
```

### Trajectories of the mean value of the 3 genes through time:


```{r,fig.width=10,fig.height=5,cache=TRUE,fig.cap="Low rate of change (vt = 0.002) and $\\sigma_z = 2\\times\\sigma_y$ parameter: $m = .2 , vt = .002, \\omega= 2, K = 2000 \\delta=.5$"}
par(mfrow=c(1,3))
for(m in unique(moving_theta$mu)){
    #for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .002 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==2 & moving_theta$delta == .5,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        for(i in sigmas){
            for(j in ks){
                plot3Genes(uniquesetup[uniquesetup$sigma==i & uniquesetup$k_y==j,],side="topleft",main=bquote( mu == .(m) ~ k[z] == 2 ~ k[y]== .(j) ~ sigma[s] == .(i)))
            }
        }
    #}
}
```


```{r,fig.width=10,fig.height=5,cache=TRUE,fig.cap="High rate of change (vt = 0.2) and $\\sigma_z = 2\\times\\sigma_y$ Parameter: $m = .2 , vt = .2, \\omega= 2, K = 2000 , \\delta=.5$"}
par(mfrow=c(1,3))
for(m in unique(moving_theta$mu)){
    #for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .2 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==2 & moving_theta$delta == 2,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        for(i in sigmas){
            for(j in ks){
                plot3Genes(uniquesetup[uniquesetup$sigma==i & uniquesetup$k_y==j,],side="topleft",main=bquote( mu == .(m) ~ k[z] == 2 ~ k[y]== .(j) ~ sigma[s] == .(i)))
            }
        }
    #}
}
```


```{r,fig.width=10,fig.height=5,cache=TRUE,fig.cap="Low rate of change (vt = 0.002) and $\\sigma_z = 2\\times\\sigma_y$ Parameter: $m = .2 , vt = .002, \\omega= 2, K = 2000 \\delta=.5$"}
par(mfrow=c(1,3))
for(m in unique(moving_theta$mu)){
    #for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .002 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==2 & moving_theta$delta == 2,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        for(i in sigmas){
            for(j in ks){
                plot3Genes(uniquesetup[uniquesetup$sigma==i & uniquesetup$k_y==j,],side="topleft",main=bquote( mu == .(m) ~ k[z] == 2 ~ k[y]== .(j) ~ sigma[s] == .(i)))
            }
        }
    #}
}
```

 
```{r,fig.width=10,fig.height=5,cache=TRUE,fig.cap="High rate of change (vt = 0.2) and $\\sigma_z = 2\\times\\sigma_y$ Parameter: $m = .2 , vt = .2, \\omega= 2, K = 2000 , \\delta=.5$"}
par(mfrow=c(1,3))
for(m in unique(moving_theta$mu)){
    #for(var in c("distX","mean_y","mean_z")){
        uniquesetup=moving_theta[ moving_theta$m == .2 &  moving_theta$vt == .2 & moving_theta$omega == 2 & moving_theta$K == 2000 & moving_theta$mu == m & moving_theta$k_z==2 & moving_theta$delta == .5,]
        sigmas=sort(unique(uniquesetup$sigma))
        ks=sort(unique(uniquesetup$k_y))
        for(i in sigmas){
            for(j in ks){
                plot3Genes(uniquesetup[uniquesetup$sigma==i & uniquesetup$k_y==j,],side="topleft",main=bquote( mu == .(m) ~ k[z] == 2 ~ k[y]== .(j) ~ sigma[s] == .(i)))
            }
        }
    #}
}
```


## Vertical summary of theoretical environments

In this section we explore again environment artificially generated with constant $\delta$ and $\omega$ (cf section \@ref(environments)) but this time instead of just looking at the final step as we did [here](http://www.dysoc.org/~simon/report/interactiongenenv.html) we look for the full trajectories of individual runs.

Each section explores a different selective pressure $\sigma_s$ while the grid of picture represent changing $\delta$ and $\omega$. The mean optimum $\theta$ is kept constant during the whole simulations. Within each combination of $(\sigma_s,\delta,\omega)$, $\mu$, $k_y$ and $k_z$ are changed, where $\sigma_y=k_z\times\sigma_s$ and $\sigma_z=k_z\times\sigma_y$. Each vertical line represents a single run. 20 simulation are represented for each combination of parameter. Each simulation was ran for $7000$ generations and with a unique environment (each time  a new environment is generated).


```{r,cache=TRUE}
    namesRealEnv=c("theoretical")
    namesFun=c("interpolate_Sls")
    namesSLS=c("fitprop")
    suf="THEORAWDEL"
    folder=paste0(namesRealEnv,namesFun,namesSLS,suf)
    binded=getAllFromFolder(folder,pref="output")
    allEs=1
    allSigmas=unique(binded$sigma)
    allDeltas=unique(binded$delta)
    allOmegas=unique(binded$omega)
```

```{r,cache=TRUE}
omegadeltacombined=paste0("$(\\omega,\\delta)=(" ,apply(expand.grid(omega=allOmegas,delta=allDeltas),1,paste0,collapse=","),")$" ,collapse=" ," )
basecaption2=paste0('($\\\\sigma_s={{s}}$, from top to bottom $\\\\delta = (',paste0(allDeltas,collapse=","),')$ from left to right $\\\\omega = (',paste0(allOmegas,collapse=","),')$, real E: $E_x=0.1,E_y=1,E_z=.125$, Theoretical env, )')
basecaption1=basecaption2
ow="25%"  #figure width
a=c(
    "### Explore $\\sigma_s = {{s}}$ ",
    '```{r}',
    'idexpe="{{id}}"',
    'allimg=unlist(lapply(allDeltas,function(d)unlist(lapply(allOmegas,function(o)paste0("images/verticalMz_",idexpe,"_sigma",{{s}},"_E1","_delta",d,"_omega",o,".png")))))',
    '```',
    "#### Vertical run indiv",
    paste0('```{r traj-ind-{{id}}-{{s}}, out.width = "',ow,'", fig.cap="Vertical trajectories for 20 individual runs',basecaption2,'",fig.show="hold",fig.link=allimg}'),
    'idexpe="{{id}}"',
    'knitr::include_graphics(as.character(allimg))',
    '```',
    '```{r}',
    'allimg=unlist(lapply(allDeltas,function(d)unlist(lapply(allOmegas,function(o)paste0("images/vertical_N_Mz_",idexpe,"_sigma",{{s}},"_E1","_delta",d,"_omega",o,".png")))))',
    '```',
    "#### Vertical run indiv: $N_e$",
    paste0('```{r traj-indN-{{id}}-{{s}}, out.width = "',ow,'", fig.cap="Vertical trajectories for 20 individual runs. Color correspond to $N_e$, the greener, the closer to K, K=1000.',basecaption2,'",fig.show="hold",fig.link=allimg}'),
    'idexpe="{{id}}"',
    'knitr::include_graphics(as.character(allimg))',
    '```'
    )

```

```{r,include=FALSE,echo=T,cache=TRUE}
src <- lapply(allSigmas,function(s)knitr::knit_expand(text=a,id=folder,s=s))

```

`r knitr::knit_child(text=unlist(src))`

```{r,cache=TRUE}
    namesRealEnv=c("theoretical")
    namesFun=c("interpolate_Sls")
    namesSLS=c("fitprop")
    suf="ICOND"
    folder=paste0(namesRealEnv,namesFun,namesSLS,suf)
    binded=getAllFromFolder(folder,pref="output")
    allEs=1
    allSigmas=unique(binded$sigma)
    allDeltas=unique(binded$delta)
    allOmegas=unique(binded$omega)
```

```{r,cache=TRUE}
omegadeltacombined=paste0("$(\\omega,\\delta)=(" ,apply(expand.grid(omega=allOmegas,delta=allDeltas),1,paste0,collapse=","),")$" ,collapse=" ," )
basecaption2=paste0('($\\\\sigma_s={{s}}$, from top to bottom $\\\\delta = (',paste0(allDeltas,collapse=","),')$ from left to right $\\\\omega = (',paste0(allOmegas,collapse=","),')$, real E: $E_x=0.1,E_y=1,E_z=.125$, Theoretical env, )')
basecaption1=basecaption2
ow="25%"  #figure width
a=c(
    "### Explore $\\sigma_s = {{s}}$ ",
    '```{r}',
    'idexpe="{{id}}"',
    'allimg=unlist(lapply(allDeltas,function(d)unlist(lapply(allOmegas,function(o)paste0("images/verticalMz_",idexpe,"_sigma",{{s}},"_E1","_delta",d,"_omega",o,".png")))))',
    '```',
    "#### Vertical run indiv",
    paste0('```{r traj-ind-{{id}}-{{s}}, out.width = "',ow,'", fig.cap="Vertical trajectories for 20 individual runs',basecaption2,'",fig.show="hold",fig.link=allimg}'),
    'idexpe="{{id}}"',
    'knitr::include_graphics(as.character(allimg))',
    '```',
    '```{r}',
    'allimg=unlist(lapply(allDeltas,function(d)unlist(lapply(allOmegas,function(o)paste0("images/vertical_N_Mz_",idexpe,"_sigma",{{s}},"_E1","_delta",d,"_omega",o,".png")))))',
    '```',
    "#### Vertical run indiv: $N_e$",
    paste0('```{r traj-indN-{{id}}-{{s}}, out.width = "',ow,'", fig.cap="Vertical trajectories for 20 individual runs. Color correspond to $N_e$, the greener, the closer to K, K=1000.',basecaption2,'",fig.show="hold",fig.link=allimg}'),
    'idexpe="{{id}}"',
    'knitr::include_graphics(as.character(allimg))',
    '```'
    )

```

```{r,include=FALSE,echo=T,cache=TRUE}
src <- c(knitr::knit_expand(text="### Varying initial condition ",env=env),lapply(allSigmas,function(s)knitr::knit_expand(text=a,id=folder,s=s)))

```

`r knitr::knit_child(text=unlist(src))`
